cmake_minimum_required(VERSION 3.10.2)
project("GitLab Runner Demo"
   DESCRIPTION "A tutorial project to demonstrate GitLab Runner"
   LANGUAGES CXX)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_executable(HelloWorld ${CMAKE_SOURCE_DIR}/hello_world.cpp)

add_custom_target(compile_commands ALL
   COMMAND mv ${CMAKE_BINARY_DIR}/compile_commands.json compile_commands.json
   WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
